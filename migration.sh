#!/bin/bash
set -e
rm -rf migration
repos=()
#testing

totalPages=$(curl --head --header "PRIVATE-TOKEN: -q9AgEmotaDk-NeF5tX6" "https://gitlab.com/api/v4/projects/?simple=yes&private=true&per_page=100&owned=yes" | grep x-total-pages | grep -o -e [0-9])
echo "total Pages =$totalPages"
for ((i=1;i<=$totalPages;i++)); do
    repos+=$(curl -X GET --header "PRIVATE-TOKEN: -q9AgEmotaDk-NeF5tX6" "https://gitlab.com/api/v4/projects/?simple=yes&private=true&per_page=100&page=$i&owned=yes" --header "Content-Type: application/json" | jq '.[].http_url_to_repo')
done
echo "--------------------------------------"
echo "repos =$repos"
k=1
echo "For loop starting"
for repo in $repos; do
        echo "Inside loop starting"
        echo "RepoCounter =$repo"
        repoName=$(echo $repo|rev|cut -d'.' -f2-|cut -f1 -d'/'|rev)
        echo "Reponame =$repoName"
        projName=$(echo "${repo#*.com\/}"|rev|cut -d'/' -f2-|rev)
        echo "Projectname =$projName"
        projID=$(echo "${repo#*.com\/}"|rev|cut -d'/' -f2-|rev|sed 's/\//_/g')
        echo "Projectid =$projID"
        grpName=$(echo "${repo#*.com\/}"|rev|sed 's|.*\/||'|rev)
        echo "Groupname =$grpName"
        repoURL=$(echo $repo|sed "s/\"//g")
        echo "RepoURL =$repoURL"
        gRepoURL=$(echo "${repo#*.com\/}"|sed "s/\"//g"|rev|cut -d'/' -f2-|rev|sed "s/\//%2F/g")
        echo "Group Repo URL =$gRepoURL"
        gRepoID=$(curl -X GET --header "PRIVATE-TOKEN: -q9AgEmotaDk-NeF5tX6" "https://gitlab.com/api/v4/projects/${gRepoURL}%2F${repoName}" -H "Content-Type: application/json" | jq '.id')
        echo "Group Repo ID =$gRepoID"
        #FYI: -s = Don't show download progress, -o /dev/null = don't display the body, -w "%{http_code}" = Write http response code to stdout after exit
        projStatus=$(curl -s -o /dev/null -I -u dharani_bb_1981:Hello123 -w "%{http_code}" "https://api.bitbucket.org/2.0/workspaces/{dharani_bb_1981}/projects/$projID")
        echo "Project Status =$projStatus"
        if [ $projStatus -eq 200 ]
        then
            echo "Project with this key:$projID  already exists in Bitbucket."
        else
        bproj=$(cat << EOF
{
        "name": "${projName}",
        "key": "${projID}",
        "description": "Software for colonizing mars.",
        "is_private": false
}
EOF
)
        curl -X POST -u dharani_bb_1981:Hello123 "https://api.bitbucket.org/2.0/workspaces/{dharani_bb_1981}/projects" -H "Content-Type: application/json" -d "$bproj" | jq .
        fi
        mkdir migration
        cd migration
        git clone --bare $repoURL
        repoStatus=$(curl -s -o /dev/null -I -u dharani_bb_1981:Hello123 -w "%{http_code}" "https://api.bitbucket.org/2.0/repositories/{dharani_bb_1981}/$repoName")
        echo $repoStatus
        if [ "$repoStatus" -eq 200 ]
        then
            echo "Repository:$repoName already exists in Bitbucket"
        else
        brepo=$(cat << EOF
{
        "scm": "git",
        "is_private": false,
        "project": {
            "key": "${projID}"
        }
}
EOF
)
        curl -X POST -u dharani_bb_1981:Hello123 "https://api.bitbucket.org/2.0/repositories/{dharani_bb_1981}/$repoName" -H "Content-Type: application/json" -d "$brepo" | jq .
        fi
        cd $repoName.git
        ls
        git push --mirror https://dharani_bb_1981:Hello123@bitbucket.org/dharani_bb_1981/$repoName.git
        cd ..
        rm -rf $repoName.git
        cd ..
        rm -rf migration
        ((k+=1))
        echo "Total Repositories migrated are = $k"
        subject="Repo migrated count"
        body="Total no of repos migrated ${k}"
        from="dkanumuri@altimetrik.com"
          to="dkanumuri@altimetrik.com,pkhadake@altimetrik.com"
          echo -e "Subject:${subject}\n${body}" | sendmail -f "${from}" -t "${to}"
done
echo "End of the Migration"

